﻿var reader = require('php-parser');
var fs = require('fs');
var pr = require('./pretty');
var tr = require('./transform');

var src = '../sample/array.php';
var dst = '../sample/array.out.php';


var code = fs.readFileSync(src, 'utf-8');
var ast = reader.parseCode(code);

var accept = {
    'prop': function (code) {
        if (code.value.value == 'length') {
            let fn = { type: 'ns', position: null, args: [ 'count' ] };
            return {type: 'call', fn: fn, args: [code.obj]};
        }
        return code;
    },

    'call': function (code) {
        if (code.fn.type == 'prop') {
            var prop = code.fn;
            var fn_map = {
                'keys' : 'array_keys', 
                'map' : 'array_map', 
                'filter': 'array_filter', 
                'reduce': 'array_reduce',
                'forEach': 'array_walk',
                'reverse': 'array_reverse',
                'substr': 'substr',
                'replace': 'preg_replace',
                'split': 'explode',
                'join': 'implode'
            };

            if (fn_map.hasOwnProperty(prop.value.value)) {
                let fn = { type: 'ns', position: null, args: [ fn_map[prop.value.value] ] };
                if (['map', 'replace', 'split', 'join'].indexOf(prop.value.value) >= 0) {
                    return {type: 'call', fn: fn, args: code.args.concat(prop.obj)};
                }
                return {type: 'call', fn: fn, args: [prop.obj].concat(code.args)};
            }
        }
        return code;
    }
};

var newast = tr.transform(ast, accept);
fs.writeFileSync(dst, pr.print_php(newast));
