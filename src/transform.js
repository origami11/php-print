﻿/* bottom up */
function transform(code, rules) {    
    if (Array.isArray(code)) {
        return code.map(x => transform(x, rules));
    }

    if (!code || !code.hasOwnProperty('type')) {
        return code;
    }

    var result = {};    
    Object.keys(code).forEach((key) => {
        if (key == 'type' || key == 'position') {
            result[key] = code[key];
        } else {
            result[key] = transform(code[key], rules);
        }
    });

    if (rules.hasOwnProperty(code.type)) {
        result = rules[code.type](result);
    }

    return result;    
}

/* top down */
function walk_keys(code, rules) {
    var result = {};    
    Object.keys(code).forEach((key) => {
        if (key == 'type' || key == 'position') {
            result[key] = code[key];
        } else {
            result[key] = walk(code[key], rules);
        }
    });
    return result;
}

function walk(code, rules) {    
    if (Array.isArray(code)) {
        return code.map(x => walk(x, rules));
    }

    if (!code || !code.hasOwnProperty('type')) {
        return code;
    }

    if (rules.hasOwnProperty(code.type)) {
        return rules[code.type](code);
    }

    return walk_keys(code, rules);
}

exports.transform = transform
exports.walk = walk;
exports.walk_keys = walk_keys;
