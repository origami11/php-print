﻿var reader = require('php-parser');
var fs = require('fs');
var pr = require('./pretty');
var tr = require('./transform');

var src = '../sample/fn.php';
var dst = '../sample/fn.out.php';

var code = fs.readFileSync(src, 'utf-8');
var ast = reader.parseCode(code);

class Scope {
    constructor() {
        this.stack = [];
    }  

    begin() {
        this.stack.push({local: [], use: []});
    }

    end() {
        this.stack.pop();
    }

    top() {
        return this.stack[this.stack.length - 1];
    }

    set(name) {
//        console.log('set', name);
        var list = this.stack[this.stack.length - 1].local;
        if (!list.includes(name)) {
            list.push(name);
        }
    }

    get(name) {
//        console.log('get', name);
        for(var i = this.stack.length - 1; i > 0; i--) {
            var st = this.stack[i];
            if (st.local.includes(name) || st.use.includes(name)) {
                return true;
            }
            st.use.push(name);            
        }
       
        return false;
    }
}

var accept_fn = {
    'function': function (code) {
        scope.begin();
        code.args.forEach(function (arg) {
            scope.set(arg.name);
        });

        var result = tr.walk_keys(code, accept_fn);        

        result.use = scope.top().use.map(function (x) { return [false, x]; });
        scope.end();

//        console.log(result);
        return result;
    },
    'set': function (code) {        
        scope.set(code.target.name);
        return code;
    },
    'var': function (code) {        
        scope.get(code.name);
        return code;
    }
};

let scope = new Scope();
scope.begin();
let newast = tr.walk(ast, accept_fn);
scope.end();

fs.writeFileSync(dst, pr.print_php(newast));
