<?php

$x = array(1, 2, 3);
print_r(array_filter(array_map(function ($n) {
    return $n * 2;
}, $x), function ($n) {
    return $n > 2;
}));
echo count($x), "\n";
$z = "abc";
echo preg_replace("/b/", "z", $z), "\n";
$f = "a.b.c";
echo implode(",", explode(".", $f)), "\n";
