<?php

require_once 'headers.php';

if (substr($_POST[name], 0, 4) == "oou-") {
    $_POST[name] = "ouo-" . substr($_POST[name], 4);
}
srand(time(0));
if ($_GET['name'] && $_GET['code']) {
    $_POST['name'] = $_GET['name'];
    $_POST['code'] = $_GET['code'];
}
if (strlen($_GET['do']) > 32) {
    $_GET['do'] = substr($_GET['do'], 0, 32);
}

$session = new Session();

if ($session->mylevel() <= 2 && $_GET['do'] == "elmon") {
    $_GET['access'] = "restrict";
}

if ($_GET['do'] == "logout" && $session->sid == $_GET['sid']) {
    $session->close();
} elseif ($_GET['do'] == "speedsave" 
    && $session->sid == $_GET['sid'] 
    && $session->mylevel() > $session->config->authlevels['not_a_user'] 
    && ($speed = intval($_GET['speed'])) 
    && !preg_match($session->config->pcre_nonstatip, pg_escape_string(getenv('REMOTE_ADDR')))
) {
    $ping = intval($_GET['ping']);
    header("content-type: text/plain");
    if (($lspeed = intval($_GET['lspeed']))) {
        $lping = intval($_GET['lping']);
        $session->database->query(
            "INSERT INTO sir_speedtest (inet_speed,id_user,ip,yarnet_speed,useragent,inet_ping,yarnet_ping) " .
            "VALUES ($speed,(SELECT user_id FROM sir_sessions  " .
            "WHERE sid='" . $session->sid . "')," .
            "'" . pg_escape_string(getenv('REMOTE_ADDR')) . "',$lspeed," .
            "'" . pg_escape_string(getenv('HTTP_USER_AGENT')) . "',$ping,$lping)"
        );
    } else {
        $session->database->query(
            "INSERT INTO sir_speedtest (inet_speed,id_user,ip,useragent,inet_ping) " .
            "VALUES ($speed,(SELECT user_id FROM sir_sessions  " .
            "WHERE sid='" . $session->sid . "')," .
            "'" . pg_escape_string(getenv('REMOTE_ADDR')) . "'," .
            "'" . pg_escape_string(getenv('HTTP_USER_AGENT')) . "',$ping)"
        );
    }
    echo "OK";
} elseif (!aja()) {
    if (ismouo()) {
        $session->config->local->auth_level['rating'] = $session->config->local->rights_level['rating'] = 5;
    }
    echo main();
}

$session->log('php,portal');

$session->database->close();
