<?php

$x = array(1, 2, 3);

print_r($x->map(function ($n) { return $n * 2; })->filter(function ($n) { return $n > 2; }));

echo $x->length, "\n";

$z = "abc";

echo $z->replace('/b/', 'z'), "\n";

$f = "a.b.c";

echo $f->split(".")->join(","), "\n";